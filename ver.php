<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Aufgabe Verschlüsselung V_1.0</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0"
            crossorigin="anonymous"></script>
</head>
<body>
<div class="container">
    <h1>Verschlüsselung v 1</h1>
    <!-- unser Form -->
    <!--  Texteingabe (1-n Zeichen), Zahleneingabe (1-25) und
            zwei Absende Button (Verschlüsseln/Entschlüsseln).   -->
    <form action="ver.php" method="post">
        <input type="text" name="texteingabe"  placeholder="Gebe ein Text (1-n Zeichen) ein">
        <input type="text" name="zahleneingabe"  placeholder="Gebe eine Zahle (1-25) ein">
        <br><br>
        <button type="submit" name="verschlüsseln" class="btn btn-primary">Verschlüsseln</button>
        <button type="submit" name="entschlüsseln" class="btn btn-primary">Entschlüsseln</button>


<?php
if(isset($_POST["verschlüsseln"]) || isset($_POST["entschlüsseln"]))
{
    if($_POST["texteingabe"] =="" || $_POST["zahleneingabe"] =="")
        echo "<br><h1><b><p style='color:blue'>Füllen Sie bitte alle Felder aus !!!</b></p></h1>";

    else
    {
        $str = strtoupper($_POST["texteingabe"]);
        $zal = $_POST["zahleneingabe"];

        $patterns = ['/Ä/', '/Ö/', '/Ü/', '/ß/', '/ä/', '/ö/', '/ü/'];
        $replacements = ['AE', 'OE', 'UE', 'SS', 'AE', 'OE', 'UE'];

        $str = preg_replace($patterns, $replacements, $str);

        $var = str_split($str, 1);
        echo "<H1>Jede Buchstabe wird um die $zal Stellen verschoben<BR> </H1>";

        if (isset($_POST["verschlüsseln"])) {
            $vrsl = "";
            foreach ($var as $a) {
                //  if(IntlChar::iisalpha($a))
                if (preg_match('/[A-Z]/', $a)) {
                    $v = ord($a) + $zal;
                    $vrsl .= chr($v > 90 ? ($v - 26) : $v);
                } else
                    $vrsl .= $a;
            }
        } else if (isset($_POST["entschlüsseln"])) {
            $vrsl = "";
            foreach ($var as $a) {
                if (preg_match('/[A-Z]/', $a)) {
                    $v = ord($a) - $zal;
                    $vrsl .= chr($v < 65 ? ($v + 26) : $v);
                } else
                    $vrsl .= $a;
            }
        }

        $tmp = !isset($_POST["textarea"]) ? "" . $str : $_POST["textarea"] . $str;
        $tmp .= !isset($_POST["verschlüsseln"]) ? " <= " : " => ";
        $tmp .= $zal . " == " . $vrsl . " \n----------------------\n";

        echo "<p><b>Verschlüsselte mit dem Caesar Algorithmus Text :</b></p>";
        echo "<textarea rows='10' cols='100' name='textarea'> $tmp </textarea>";
    }
}
?>

    </form>
</div>
</body>
